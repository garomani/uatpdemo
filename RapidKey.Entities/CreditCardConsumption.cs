﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RapidKey.Entities
{
    public class CreditCardConsumption : Entity
    {
        [ForeignKey("CreditCard")]
        public int CreditCardId { get; set; }
        public CreditCard CreditCard { get; set; }

        [Required]
        public DateTime TransactionDate { get; set; }

        [Required]
        public decimal TransactionAmount { get; set; }

        public decimal TransactionFee { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public int TransactionType { get; set; }
    }
}
