﻿namespace RapidKey.Entities
{
    public static class ConsumptionType
    {
        public const int Credit = 1;
        public const int Debit = -1;
    }
}
