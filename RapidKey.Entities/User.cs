﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace RapidKey.Entities
{
    public class User : IdentityUser
    {
        [MaxLength(50)]
        public string? Name { get; set; }

        public ICollection<CreditCard> CreditCards { get; set; }
    }
}
