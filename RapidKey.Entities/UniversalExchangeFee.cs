﻿namespace RapidKey.Entities
{
    public class UniversalExchangeFee : Entity
    {
        public double FeeIndex { get; set; }

        public bool IsActive { get; set; }
    }
}
