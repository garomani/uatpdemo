﻿using System.ComponentModel.DataAnnotations.Schema;

namespace RapidKey.Entities
{
    public class CreditCard : Entity
    {
        [ForeignKey("User")]
        public string UserId { get; set; }
        public virtual User User { get; set; }

        public string AccountNumber { get; set; }

        public string Number { get; set; }

        public string VerificationCode { get; set; }

        public DateTime ExpirationDate { get; set; }

        public decimal LimitAmount { get; set; }


    }
}
