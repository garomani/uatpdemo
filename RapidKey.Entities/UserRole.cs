﻿namespace RapidKey.Entities
{
    public static class UserRole
    {
        /// <summary>
        /// The role name for the administrator.
        /// </summary>
        public const string Admin = "Admin";

        /// <summary>
        /// The role name for the financial employees.
        /// </summary>
        public const string Financial = "Financial";

        /// <summary>
        /// The role name for regular users.
        /// </summary>
        public const string User = "User";
    }

}
