﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace RapidKey.Entities.Context
{
    public class RapidKeyContext : IdentityDbContext<User>
    {
        public DbSet<User> Users { get; set; }
        public DbSet<CreditCard> CreditCards { get; set; }
        public DbSet<UniversalExchangeFee> UniversalExchangeFees { get; set; }
        public DbSet<CreditCardConsumption> CreditCardConsumptions { get; set; }


        public RapidKeyContext(DbContextOptions<RapidKeyContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<CreditCard>()
                .HasIndex(c => c.AccountNumber)
                .IsUnique();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);

        }
    }
}
