﻿using System.ComponentModel.DataAnnotations;

namespace RapidKey.Entities
{
    public abstract class Entity
    {
        [Key]
        public int Id { get; set; }
    }
}
