using Hangfire;
using Hangfire.SqlServer;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using RapidKey.Entities;
using RapidKey.Entities.Context;
using RapidKey.Repositories;
using RapidKey.Repositories.Interfaces;
using RapidKey.Services;
using RapidKey.Services.Interfaces;
using RapidKey.Services.Mappers;
using System.Text;


var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();


builder.Services.AddDbContext<RapidKeyContext>(opt =>
        opt.UseSqlServer(builder.Configuration.GetConnectionString("RapidKey")));

builder.Services.AddIdentity<User, IdentityRole>()
                .AddEntityFrameworkStores<RapidKeyContext>()
                .AddDefaultTokenProviders();

builder.Services.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
        })
        .AddJwtBearer(options =>
        {
            options.SaveToken = true;
            options.RequireHttpsMetadata = false;
            options.TokenValidationParameters = new TokenValidationParameters()
            {
                ValidateIssuer = true,
                ValidateAudience = true,
                ValidAudience = builder.Configuration["AppSettings:JwtValidAudience"],
                ValidIssuer = builder.Configuration["AppSettings:JwtValidIssuer"],
                ClockSkew = TimeSpan.Zero,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["AppSettings:JwtTokenSecret"]))
            };
        });

builder.Services.Configure<AppSettings>(builder.Configuration.GetSection(""));

var connectionString = builder.Configuration.GetConnectionString("RapidKey");
builder.Services.AddHangfire(configuration => configuration
                .SetDataCompatibilityLevel(CompatibilityLevel.Version_170).UseSimpleAssemblyNameTypeSerializer()
                .UseRecommendedSerializerSettings().UseSqlServerStorage(connectionString, new SqlServerStorageOptions
                {
                    CommandBatchMaxTimeout = TimeSpan.FromMinutes(5),
                    SlidingInvisibilityTimeout = TimeSpan.FromMinutes(5),
                    QueuePollInterval = TimeSpan.Zero,
                    UseRecommendedIsolationLevel = true,
                    DisableGlobalLocks = true
                }));
JobStorage.Current= new SqlServerStorage(connectionString);
builder.Services.AddHangfireServer();


RecurringJob.AddOrUpdate<IUniversalFeesExchangeService>("UFE Service", ufe => ufe.updateFeeExchange(), Cron.Hourly);

builder.Services.AddTransient<IAuthService, AuthService>();
builder.Services.AddTransient<ICreditCardRepository, CreditCardRepository>();
builder.Services.AddTransient<ICreditCardService, CreditCardService>();
builder.Services.AddTransient<IUniversalExchangeFeeRepository, UniversalExchangeFeeRepository>();
builder.Services.AddTransient<IUniversalFeesExchangeService, UniversalFeesExchange>();
builder.Services.AddTransient<ICreditCardConsumptionRepository, CreditCardConsumptionRepository>();


builder.Services.AddAutoMapper(typeof(RapidKeyProfile));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
