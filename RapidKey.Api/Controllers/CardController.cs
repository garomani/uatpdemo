﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using RapidKey.Services.Dto;
using RapidKey.Services.Interfaces;
using System.Security.Claims;

namespace RapidKey.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CardController : ControllerBase
    {
        private readonly ICreditCardService _creditCardService;
        public CardController(ICreditCardService creditCardService)
        {
            _creditCardService = creditCardService;
        }

        [HttpPost]
        public async Task<ActionResult<CreditCardDto>> CreateCard(CreditCardDto model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid payload");
            }

            var identity = HttpContext.User.Identity as ClaimsIdentity;
            var userName = string.Empty;
            if (identity != null)
            {
                userName = identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Name)?.Value;
            }
            var (status, message, result) = await _creditCardService.AddCreditCard(model, userName);


            if (status == 0)
            {
                return BadRequest(message);
            }
            return Ok(result);
        }

        [HttpPost("/pay")]
        public async Task<ActionResult<ConsumptionDto>> SaveConsumption(ConsumptionDto payment)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Invalid payload");
            }

            var (status, message, result) = await _creditCardService.SaveConsumption(payment);

            if (status == 0)
            {
                return BadRequest(message);
            }
            return Ok(result);
        }

        [HttpGet("balance/{id}")]
        public async Task<ActionResult<CreditCardBalanceDto>> GetBalance(int id)
        {
            var balance = await _creditCardService.GetCreditCardBalance(id);
            if ( balance == null)
            {
                return BadRequest();
            }
            return Ok(balance);
        }
    }
}
