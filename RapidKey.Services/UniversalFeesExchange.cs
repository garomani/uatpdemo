﻿using RapidKey.Entities;
using RapidKey.Repositories.Interfaces;
using RapidKey.Services.Interfaces;

namespace RapidKey.Services
{
    public class UniversalFeesExchange : IUniversalFeesExchangeService
    {
        private readonly IUniversalExchangeFeeRepository _repository;
        private Random _random;
        public UniversalFeesExchange(IUniversalExchangeFeeRepository service)
        {

            _repository = service;
            _random = new Random();
        }

        public async Task updateFeeExchange()
        {
            var index = (_random.NextDouble() * 2) + 0;
            var entity = new UniversalExchangeFee
            {
                FeeIndex = index,
                IsActive = true
            };

            var lastIndex = await _repository.GetLastActive();
            if (lastIndex != null)
            {
                lastIndex.IsActive = false;
                await _repository.Update(entity);
            }
            await _repository.Add(entity);
        }
    }
}
