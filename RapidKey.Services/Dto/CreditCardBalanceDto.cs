﻿using RapidKey.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RapidKey.Services.Dto
{
    public class CreditCardBalanceDto
    {
        public string ClientName { get; set; }
        public string AccountNumer { get; set; }
        public DateTime BalanceDate { get; set; }
        public ICollection<ConsumptionDto> Consumptions { get; set; }
    }
}
