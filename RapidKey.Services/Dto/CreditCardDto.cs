﻿using RapidKey.Services.Attributes;
using System.ComponentModel.DataAnnotations;

namespace RapidKey.Services.Dto
{
    public class CreditCardDto
    {
        [Required]
        [CreditCardNumber]
        public string Number { get; set; }

        [Required]
        [VerificationCode]
        public string VerificationCode { get; set; }

        [Required]
        public DateTime ExpirationDate { get; set; }

        [Required]
        public double LimitAmount { get; set; }
    }
}
