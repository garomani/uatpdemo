﻿using RapidKey.Services.Attributes;
using System.ComponentModel.DataAnnotations;

namespace RapidKey.Services.Dto
{
    public class ConsumptionDto
    {
        [Required]
        [CreditCardNumber]
        public string Number { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value bigger than {1}")]
        public decimal Amount { get; set; }

        public decimal FeeAmount { get; set; }

        [Required]
        public DateTime ConsumptionDate { get; set; }

        [Required]
        public int ConsumptionTye { get; set; }
    }
}
