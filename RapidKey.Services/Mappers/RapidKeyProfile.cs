﻿using AutoMapper;
using RapidKey.Entities;
using RapidKey.Services.Dto;
using RapidKey.Services.Utilities;

namespace RapidKey.Services.Mappers
{
    public class RapidKeyProfile : Profile
    {
        public RapidKeyProfile()
        {
            CreateMap<CreditCardDto, CreditCard>();
            CreateMap<CreditCard, CreditCardDto>();

            CreateMap<ConsumptionDto, CreditCardConsumption>()
                .ForMember(dest => dest.TransactionDate, opt => opt.MapFrom(s => s.ConsumptionDate))
                .ForMember(dest => dest.TransactionType, opt => opt.MapFrom(s => s.ConsumptionTye))
                .ForMember(dest => dest.TransactionAmount, opt => opt.MapFrom(s => s.Amount));

            CreateMap<CreditCardConsumption, ConsumptionDto>()
                .ForMember(dest => dest.Number, opt => opt.MapFrom(s => s.CreditCard.Number))
                .ForMember(dest => dest.ConsumptionTye, opt => opt.MapFrom(s => s.TransactionType))
                .ForMember(dest => dest.ConsumptionDate, opt => opt.MapFrom(s => s.TransactionDate))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(s => s.TransactionAmount))
                .ForMember(dest => dest.FeeAmount, opt => opt.MapFrom(s => s.TransactionFee));



        }
    }
}
