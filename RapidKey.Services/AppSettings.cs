﻿namespace RapidKey.Services
{
    public class AppSettings
    {
        public string? PasswordSecret { get; set; }
        public string? JwtTokenSecret { get; set; }
        public string? JwtValidAudience { get; set; }
        public string? JwtValidIssuer { get; set; }
    }
}
