﻿using System.ComponentModel.DataAnnotations;

namespace RapidKey.Services.Attributes
{
    public class VerificationCodeAttribute : ValidationAttribute
    {
        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return ValidationResult.Success;
            }

            string ccValue = value as string;
            if (ccValue == null)
            {
                return new ValidationResult("The credit card verification code can not be empty");
            }

            if (ccValue.Length >= 3 && ccValue.Length < 5)
            {
                return new ValidationResult("The credit card number verficiation code is not in the appropiated format of 3 or 4 digits length"); ;
            }

            int checksum = 0;
            bool evenDigit = false;

            foreach (char digit in ccValue.Reverse())
            {
                if (digit < '0' || digit > '9')
                {
                    return new ValidationResult("The credit card number verficiation code can not contains other character rather then digits"); ;
                }
            }

            return ValidationResult.Success;
        }
    }
}
