﻿using System.ComponentModel.DataAnnotations;

namespace RapidKey.Services.Attributes
{
    public class CreditCardNumberAttribute : ValidationAttribute
    {
        public CreditCardNumberAttribute()
        {

        }

        protected override ValidationResult? IsValid(object? value, ValidationContext validationContext)
        {
            if (value == null)
            {
                return ValidationResult.Success;
            }

            string ccValue = value as string;
            if (ccValue == null)
            {
                return new ValidationResult("The credit card number can not be empty");
            }

            ccValue = ccValue.Replace("-", "");
            ccValue = ccValue.Replace(" ", "");

            if (ccValue.Length < 15)
            {
                return new ValidationResult("The credit card number not in the appropiated format of 15 digits at least"); ;
            }

            int checksum = 0;
            bool evenDigit = false;

            foreach (char digit in ccValue.Reverse())
            {
                if (digit < '0' || digit > '9')
                {
                    return new ValidationResult("The credit card number can not contains other character rather then digits"); ;
                }

                int digitValue = (digit - '0') * (evenDigit ? 2 : 1);
                evenDigit = !evenDigit;

                while (digitValue > 0)
                {
                    checksum += digitValue % 10;
                    digitValue /= 10;
                }
            }

            return (checksum % 10) == 0 ? ValidationResult.Success : new ValidationResult("The credit card number is not valir");
        }
    }
}
