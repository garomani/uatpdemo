﻿namespace RapidKey.Services.Interfaces
{
    public interface IUniversalFeesExchangeService
    {
        Task updateFeeExchange();
    }
}
