﻿using RapidKey.Services.Dto;

namespace RapidKey.Services.Interfaces
{
    public interface ICreditCardService
    {
        Task<(int status, string message, CreditCardDto? model)> AddCreditCard(CreditCardDto model, string userName);
        Task<CreditCardBalanceDto> GetCreditCardBalance(int creditCarId);
        Task<(int status, string message, ConsumptionDto? result)> SaveConsumption(ConsumptionDto payment);
    }
}
