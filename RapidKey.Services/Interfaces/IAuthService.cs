﻿using RapidKey.Services.Dto;

namespace RapidKey.Services.Interfaces
{
    public interface IAuthService
    {
        Task<(int, string)> Registeration(UserDto model, string role);
        Task<(int, string)> Login(LoginDto model);
    }
}
