﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using RapidKey.Entities;
using RapidKey.Repositories.Interfaces;
using RapidKey.Services.Dto;
using RapidKey.Services.Interfaces;
using RapidKey.Services.Utilities;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace RapidKey.Services
{
    public class CreditCardService : ICreditCardService
    {
        private readonly ICreditCardRepository _creditCardRepository;
        private readonly ICreditCardConsumptionRepository _creditCardConsumptionRepository;
        private readonly IUniversalExchangeFeeRepository _universeExchangeFeeRepository;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;

        public CreditCardService(UserManager<User> userManager,
                                IOptions<AppSettings> options,
                                IMapper mapper,
                                ICreditCardRepository creditCardRepository,
                                ICreditCardConsumptionRepository creditCardConsumptionRepository,
                                IUniversalExchangeFeeRepository universalExchangeFeeRepository)

        {
            _creditCardRepository = creditCardRepository;
            _userManager = userManager;
            _mapper = mapper;
            _appSettings = options.Value;
            _creditCardConsumptionRepository = creditCardConsumptionRepository;
            _universeExchangeFeeRepository = universalExchangeFeeRepository;
        }

        public async Task<(int status, string message, CreditCardDto? model)> AddCreditCard(CreditCardDto model, string userName)
        {
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
                return (0, "User does not exists", null);

            var entity = _mapper.Map<CreditCard>(model);
            entity.UserId = user.Id;
            entity.Number = PasswordEncrypter.EncryptPassword(model.Number, _appSettings.PasswordSecret);
            entity.VerificationCode = PasswordEncrypter.EncryptPassword(model.VerificationCode, _appSettings.PasswordSecret);

            try
            {
                await _creditCardRepository.Add(entity);
                return (1, "Credit Card successfully added", model);
            }
            catch (Exception ex)
            {
                return (1, ex.Message, null);
            }
        }

        public async Task<CreditCardBalanceDto> GetCreditCardBalance(int creditCarId)
        {
            var creditCard = await _creditCardRepository.GetById(creditCarId);
            if (creditCard == null)
                return null;

            var balance = new CreditCardBalanceDto
            {
                AccountNumer = creditCard.Number,
                ClientName = creditCard.User.Name,
                BalanceDate = DateTime.Now
            };

            var consumptions = await _creditCardConsumptionRepository.GetAllByCreditCardIdAsync(creditCarId);
            balance.Consumptions = _mapper.Map< ICollection<CreditCardConsumption>, ICollection <ConsumptionDto>>(consumptions);
            return balance;
        }

        public async Task<(int status, string message, ConsumptionDto? result)> SaveConsumption(ConsumptionDto payment)
        {
            // First Check Date of Payment is less or equel to Expiration Date
            var encryptedNumber = PasswordEncrypter.EncryptPassword(payment.Number, _appSettings.PasswordSecret);
            var creditCard = await _creditCardRepository.FindByNumberAsync(encryptedNumber);

            if (creditCard == null)
                return (0, "Invalid Credit Card Number", null);

            if (creditCard.ExpirationDate < payment.ConsumptionDate)
                return (0, "Invalid Use of Credit Card. It has expire", null);

            var starDate = new DateTime(payment.ConsumptionDate.Year, payment.ConsumptionDate.Month, 1);
            var endDate = starDate.AddMonths(1).AddDays(-1);

            var totalConsumeInMonth = await _creditCardConsumptionRepository.GetTotalConsumptionInMonthAsync(creditCard.Id, starDate, endDate);

            if (totalConsumeInMonth + payment.Amount > creditCard.LimitAmount)
                return (0, "Invalid Use of Credit Card. The Total Paymnents for the month exceed the credit card limit", null);

            // Save Payment
            var entity = _mapper.Map<CreditCardConsumption>(payment);
            entity.Id = creditCard.Id;
            var feeAmount = 0m;

            if (payment.ConsumptionTye == ConsumptionType.Credit)
            {
                var lastIndex = await _universeExchangeFeeRepository.GetLastActive();
                var feeIndex = lastIndex != null ? lastIndex.FeeIndex : 0;
                var lastFeeAmount = await _creditCardConsumptionRepository.GetLastFeeAmountAsync(creditCard.Id);
                feeAmount = lastFeeAmount == 0 ? payment.Amount * 0.001m : lastFeeAmount * (decimal)feeIndex;
            }
            
            entity.TransactionFee = feeAmount;

            try
            {
                await _creditCardConsumptionRepository.Add(entity);
                return (1, "Paymnt Added", _mapper.Map<ConsumptionDto>(entity));
            }
            catch (Exception e)
            {
                return (0, e.Message, null);
            }


        }
    }
}
