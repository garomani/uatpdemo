﻿using RapidKey.Entities;
using RapidKey.Entities.Context;
using RapidKey.Repositories.Interfaces;

namespace RapidKey.Repositories
{
    public class UniversalExchangeFeeRepository : Repository<UniversalExchangeFee, int>, IUniversalExchangeFeeRepository
    {
        public UniversalExchangeFeeRepository(RapidKeyContext context)
            : base(context)
        {

        }

        public async Task<UniversalExchangeFee> GetLastActive()
        {
            var entity = _context.UniversalExchangeFees.FirstOrDefault(ufe => ufe.IsActive);
            return entity;
        }
    }
}
