﻿using Microsoft.EntityFrameworkCore;
using RapidKey.Entities;
using RapidKey.Entities.Context;
using RapidKey.Repositories.Interfaces;

namespace RapidKey.Repositories
{
    public class CreditCardRepository : Repository<CreditCard, int>, ICreditCardRepository
    {
        public CreditCardRepository(RapidKeyContext context)
            : base(context) { }

        public async Task<CreditCard> FindByNumberAsync(string encryptedNumber)
        {
            return await _context.CreditCards.FirstOrDefaultAsync(c => c.Number == encryptedNumber);
        }
    }
}
