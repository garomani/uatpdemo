﻿using Microsoft.EntityFrameworkCore;
using RapidKey.Entities;
using RapidKey.Entities.Context;
using RapidKey.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RapidKey.Repositories
{
    public class CreditCardConsumptionRepository : Repository<CreditCardConsumption, int>, ICreditCardConsumptionRepository
    {
        public CreditCardConsumptionRepository(RapidKeyContext context)
            : base(context)
        {

        }

        public async Task<ICollection<CreditCardConsumption>> GetAllByCreditCardIdAsync(int creditCardId)
        {
            var results = await _context.CreditCardConsumptions
                                        .Where(c => c.CreditCardId == creditCardId)
                                        .OrderBy(c => c.TransactionDate)
                                        .ToListAsync();
            return results;
        }

        public async Task<decimal> GetLastFeeAmountAsync(int creditCardId)
        {
            var result = await _context.CreditCardConsumptions
                                        .Where(c => c.CreditCardId == creditCardId)
                                        .OrderByDescending(c => c.TransactionDate)
                                        .FirstOrDefaultAsync();
            return result.TransactionFee;
        }

        public async Task<decimal> GetTotalConsumptionInMonthAsync(int id, DateTime starDate, DateTime endDate)
        {
            var results = await _context.CreditCardConsumptions
                                        .Where(c => c.CreditCardId == id &&
                                                    c.TransactionDate >= starDate &&
                                                    c.TransactionDate <= endDate)
                                        .ToListAsync();
            return results.Sum(p => p.TransactionAmount * p.TransactionType);

        }
    }
}
