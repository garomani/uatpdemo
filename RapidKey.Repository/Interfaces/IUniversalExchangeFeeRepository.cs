﻿using RapidKey.Entities;

namespace RapidKey.Repositories.Interfaces
{
    public interface IUniversalExchangeFeeRepository : IRepository<UniversalExchangeFee, int>
    {
        Task<UniversalExchangeFee> GetLastActive();
    }
}
