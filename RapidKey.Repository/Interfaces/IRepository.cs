﻿using RapidKey.Entities;

namespace RapidKey.Repositories.Interfaces
{
    public interface IRepository<T, Int> where T : Entity
    {
        Task<T> Add(T entity);

        Task<T> Update(T entity);

        Task<T> Remove(int id);

        Task<T> GetById(int id);

        Task<ICollection<T>> GetAll();
    }
}
