﻿using RapidKey.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RapidKey.Repositories.Interfaces
{
    public interface ICreditCardConsumptionRepository : IRepository<CreditCardConsumption, int>
    {
        Task<ICollection<CreditCardConsumption>> GetAllByCreditCardIdAsync(int creditCardId);
        Task<decimal> GetLastFeeAmountAsync(int creditCardId);
        Task<decimal> GetTotalConsumptionInMonthAsync(int creditCardId, DateTime starDate, DateTime endDate);
    }
}
