﻿using RapidKey.Entities;

namespace RapidKey.Repositories.Interfaces
{
    public interface ICreditCardRepository : IRepository<CreditCard, int>
    {
        Task<CreditCard> FindByNumberAsync(string encryptedNumber);
    }
}
