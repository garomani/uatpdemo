﻿using RapidKey.Entities;
using RapidKey.Entities.Context;
using RapidKey.Repositories.Interfaces;

namespace RapidKey.Repositories
{
    public abstract class Repository<T, Int> : IRepository<T, Int> where T : Entity
    {
        protected readonly RapidKeyContext _context;
        protected Repository(RapidKeyContext context)
        {
            _context = context;
        }

        public async Task<T> Add(T entity)
        {
            _context.Add(entity);
            _context.SaveChanges();
            return entity;
        }

        public async Task<ICollection<T>> GetAll() => _context.Set<T>().ToList();

        public async Task<T> GetById(int id) => _context.Set<T>().Where(u => u.Id == id).First();

        public async Task<T> Remove(int id)
        {
            var entity = _context.Set<T>().FirstOrDefault(u => u.Id == id);
            if (entity == null)
                throw new InvalidOperationException("The entity you want to remove does not exists");

            _context.Set<T>().Remove(entity);
            return entity;
        }

        public async Task<T> Update(T entity)
        {
            _context.Update(entity);
            _context.SaveChanges(true);
            return entity;
        }
    }
}
