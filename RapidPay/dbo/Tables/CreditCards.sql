﻿CREATE TABLE [dbo].[CreditCards] (
    [Id]               INT             IDENTITY (1, 1) NOT NULL,
    [UserId]           NVARCHAR (450)  NOT NULL,
    [Number]           NVARCHAR (MAX)  NOT NULL,
    [VerificationCode] NVARCHAR (MAX)  NOT NULL,
    [ExpirationDate]   DATETIME2 (7)   NOT NULL,
    [LimitAmount]      DECIMAL (18, 2) NOT NULL,
    [AccountNumber]    NVARCHAR (450)  DEFAULT (N'') NOT NULL,
    CONSTRAINT [PK_CreditCards] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CreditCards_AspNetUsers_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[AspNetUsers] ([Id]) ON DELETE CASCADE
);




GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_CreditCards_AccountNumber]
    ON [dbo].[CreditCards]([AccountNumber] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_CreditCards_UserId]
    ON [dbo].[CreditCards]([UserId] ASC);

