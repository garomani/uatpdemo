﻿CREATE TABLE [dbo].[UniversalExchangeFees] (
    [Id]       INT        IDENTITY (1, 1) NOT NULL,
    [FeeIndex] FLOAT (53) NOT NULL,
    [IsActive] BIT        NOT NULL,
    CONSTRAINT [PK_UniversalExchangeFees] PRIMARY KEY CLUSTERED ([Id] ASC)
);

