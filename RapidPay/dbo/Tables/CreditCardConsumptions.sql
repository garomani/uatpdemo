﻿CREATE TABLE [dbo].[CreditCardConsumptions] (
    [Id]                INT             IDENTITY (1, 1) NOT NULL,
    [CreditCardId]      INT             NOT NULL,
    [TransactionDate]   DATETIME2 (7)   NOT NULL,
    [TransactionAmount] DECIMAL (18, 2) NOT NULL,
    [TransactionFee]    DECIMAL (18, 2) NOT NULL,
    [Description]       NVARCHAR (MAX)  NOT NULL,
    [TransactionType]   INT             NOT NULL,
    CONSTRAINT [PK_CreditCardConsumptions] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CreditCardConsumptions_CreditCards_CreditCardId] FOREIGN KEY ([CreditCardId]) REFERENCES [dbo].[CreditCards] ([Id]) ON DELETE CASCADE
);




GO
CREATE NONCLUSTERED INDEX [IX_CreditCardConsumptions_CreditCardId]
    ON [dbo].[CreditCardConsumptions]([CreditCardId] ASC);

